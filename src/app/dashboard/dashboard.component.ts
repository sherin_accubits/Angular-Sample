import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private http:Http) { }
    albums: Albums[];
  ngOnInit() {
    this.http.get('https://jsonplaceholder.typicode.com/albums')
    .subscribe(
        (res:Response)=>{
          this.albums =  res.json();
        }
    )
  }

}

interface Albums{
    userId: number;
    id: number;
    title: "";
}
